#landare #zuhaixka #sendabelar #janari

- > **es.**: _saúco mayor_  
  > **en.**: _elder_
  
  ![](sambucus_nigra_ILT.jpg)
  
  + **Ordena**: _[[dipsacales]]_
  + **Familia**: _[[adoxaceae]]_
  + **Generoa**: _[[sambucus]]_
  + **Espeziea**:  _Sambucus nigra_
  
  > **Ez nahastu [[andura]]rekin** (_Sambucus ebulus_) eta [[intsusa gorri]]arekin (_Sambucus racemosa_)
  
  **Egur** biguineko **4-6 metro**ko zuhaixka, lore zuri-**horixkak** ditu eta **fruituak lurrera begira** geratzen dira.
# Non
Nahiko [[hezetasun]] dagoen tokietan hazten da.  
Basoetan, [[bide bazter]]retan eta erreka inguruetan.
# Norekin ikusia
# Erabilerak
Erabilera [[mediko]]a eta [[gastronomi]]koa ditu. [[mineral]]ak, [[C bitamina]], [[tanino]]ak, [[kolina]] eta [[alkaloide]]ak dauzka.  

Loreak, hostoak, fruituak eta adarren bigarren azala erabiltzen dira oro har.   
Loreak [[udaberri]] eta [[uda]] tartean, fruituak [[uda]] eta [[udazken]] tartean, adarren azala [[udazken]]ean. Dena gorde daiteke **urte batetik bestera**, fruituak izan ezik.
## Nagusiak 
+ [[arnas-arazo]]ak
+ [[zauri]]ak, [[erredura]]k, [[kolpe]]ak, [[herpes]]ak eta bestelako [[azal-arazo]]ak tratatzeko
+ [[sukarra]] txikitzeko, [[izerdi]]tzeko eta [[muki]]ak botatzeko
## Bestelakoak 
+ [[begi]]ak garbitzeko, [[konjuntibitis]] kasuetan edo [[bekatxo]]ak ([[orzuelo]]) daudenean
+ Aurpegiko azala leuntzeko eta poroak irekitzeko, infusioaren [[lurrin]]ekin
# Nola erabili
+ [[infusio]] moduan
	+ 20-25 gramo lore litroko, 3 minutuz irakin, 5 minutuz estalita.
	+ **Egunean** ez **2-3** aldiz baino gehiago.
+ [[ukendu]] moduan
	+ 30 gramo adarren bigarren azal litro erdi oliotan berotzen. 
+ Jateko [[marmelada]]k, [[edari freskagarri]]ak ([Socată](https://eu.wikipedia.org/wiki/Socat%C4%83)) eta abar egin daitezke.

___

[EuskalHerritarMedikuntza](https://euskalherritarmedikuntza.blogspot.com/2010/12/intsusa.html)
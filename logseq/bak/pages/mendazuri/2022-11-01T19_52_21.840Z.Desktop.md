#landare #sendabelar #janari

- > **es**. _mastranzo_ [^1]  
  > **en**. _apple mint, woolly mint_
  
  ![](mentha_suaveolens_ILT.jpg)  
  
  + **Ordena**: _[[lamiales]]_
  + **Familia**: _[[lamiaceae]]_
  + **Generoa**: _[[mentha]]_
  + **Espeziea**:  _Mentha suaveolens_ sin. _Mentha rotundifolia_
# Non
[[hezetasun]]a eta [[argi zuzena]] ditu gustuko.  
Ibai-ertzetan aurkitu ohi da.
# Norekin ikusia
+ [[marrubi]] eta [[basamarrubi]]en ondoan
+ [[sasi]] tartean
# Erabilerak
> Kantitate handietan **[[toxiko]]a** da, **ez** da gomendagarria **[[haurdun]]** daudenentzat.

Erabilera [[mediko]] eta [[gastronomi]]koak ditu.  
Hosto eta adarrak erabiltzen dira.
## Nagusiak
+ [[analgesia]]: [[sukarra]] eta [[buruko min]]a
+ [[antiseptiko]]a
+ Puzkarren aurkakoa, [[karminatibo]]a
+ [[digestibo]]a
## Bestelakoak   
+ Oaxacan [[hileko]]a erregulatzeko erabiltzen da, mendazuri infusioa hiru goizez jarraian edanez, hilekoa jetsi dadin.
- # Nola erabili
  + [[infusio]]a hiruzpalau hostorekin; adarrak ere sartu daitezke.
  + [[marmelada]]n, kuskusean eta entsaladetan erabiltzen da ere
  
  
  [^1]: https://es.wikipedia.org/wiki/Mentha_suaveolens#Nombres%20comunes
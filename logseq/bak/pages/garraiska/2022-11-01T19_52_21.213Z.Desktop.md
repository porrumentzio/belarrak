#landare #sendabelar

- > **eu.**: _arraixka_
  > **es.**: _melisa_
- ![](melissa_officinalis_ILT.jpg)  
  
  + **Ordena**: *[[lamiales]]*
  + **Familia**: *[[lamiaceae]]*
  + **Generoa**: *[[melissa]]*
  + **Espeziea**:  _Melissa officinalis_
# Non
[[lur aberats]]a, [[lur drainatu]]a eta [[hezetasun]] handiko lurrak ditu gustuko.  
Soro hezeetan, baso-soilguneetan eta ibaiertzetan.
# Erabilerak
Hostoak erabiltzen dira
## Nagusiak
+ Efektu [[lasaigarri]]a du
+ [[emozio baretzaile]]a da
+ [[espasmo]]en aurka
+ [[takikardia]]ren aurka, bihotzaren ohiko erritmoa berreskuratzeko.
- ## Bestelakoak
  + [[eltxoak]] uxatu ohi ditu
  + [[antiseptiko]]a da, horregatik [[hortzetako pasta]] egiteko erabiltzen da
  + Gosearazlea, [[orexigeno]]a
  + Puzkarren aurkakoa, [[karminatibo]]a
  + [[antiinflamatorio]]a
# Nola erabili
[[infusio]] moduan, lauzpabost hosto bizi edo lehorrekin.
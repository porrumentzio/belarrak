- #errezeta
- Hortzetako pasta deitzen diogu hortzak garbitzeko erabiltzen den oreari. Ore hori komertzialki egunerokoan ezohikoak zaizkigun materialekin egiten dira, hala nola [[fluor]]ra, [[buztin]]a, [[kuartzo]]a eta [[kaltzita]].
- ## Etxerako errezeta
- > Errezeta hau nahiko ezaguna da eta inguruko batzuk erabiltzen dugu. Edonola, oraindik ez dugu hortzen adituren baten oniritzirik. Beraz, zure ardurapean erabili.
- Bi osagai beharko ditugu:
- 1. [[koko-olio]]a
  2. [[kurkuma]] hautsa
- > Aukeran, [[sodio bikarbonato]]a gehitu ahal zaio pasta lakarragoa izateko. Halere, ingurukoek ez dute gomendatzen, [[esmalte]]a urratu dezakelako.
- [[pote]] batean[^1] nahastu koko-olioa eta kurkuma hautsa. Proportzioak ez ditut neurtuak, beraz joan probatzen zaporea, kurkuma ikutu bat nabaritu arte.
-
- [^1]: Pote bat erabili ordez, komertzial saltzen diren pasta-tutu bat hustu eta bertan sartu daiteke gure pasta. Horretarako, pasta berotu eta urtzea komeni da, gero kontu handiz tutuaren irekieran isurtzeko.
-